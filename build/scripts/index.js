import data from '../data/games.js';
const rootElement = document.querySelector('#root');
const searchButtonElement = document.querySelector('#search-button');
const searchInputElement = document.querySelector('#input-pesquisar');
const developersElement = document.querySelector('#developer');
const genresElements = document.querySelector('#genre');
const platformElements = document.querySelector('#platform');
const publisherElements = document.querySelector('#publisher');
function render(games) {
    if (rootElement) {
        rootElement.innerHTML = '';
        games.forEach((game) => {
            rootElement.innerHTML += `
      <div class="card">
      <div class="img-container"><img src="${game.thumbnail}"></div>
      <div class="card-content"> <h3>${game.title}</h3>
      <span>Platform: ${game.platform}</span>
      <span>Genre: ${game.genre}</span>
      <span>Publiser: ${game.publisher}</span>
      <span>Developer: ${game.developer}</span></div>
      <a href=${game.game_url} target="_blank" >Download</a>
      </div>
    `;
        });
    }
}
function renderFilters(itens, choosenFilter, htmlElement) {
    const filters = new Set();
    itens.forEach((game) => filters.add(game[choosenFilter]));
    const filtersArr = Array.from(filters);
    if (htmlElement) {
        htmlElement.innerHTML += '<option value=""> Select </option>';
        filtersArr.forEach((game) => {
            htmlElement.innerHTML += `
      <option id="genre" value=${game}>
       ${game}</option>
    `;
        });
    }
}
function search() {
    const searchInputValue = searchInputElement.value;
    const filterGenreValue = genresElements.value;
    const filterPlatformGenreValue = platformElements
        .value;
    const filterPublisherValue = publisherElements.value;
    const filterDeveloperValue = developersElement.value;
    const newGames = data.filter((game) => game.title.toLocaleLowerCase().includes(searchInputValue) &&
        game.genre.includes(filterGenreValue) &&
        game.platform.includes(filterPlatformGenreValue) &&
        game.developer.includes(filterDeveloperValue) &&
        game.publisher.includes(filterPublisherValue));
    render(newGames);
}
function eventListenerHandle() {
    searchButtonElement === null || searchButtonElement === void 0 ? void 0 : searchButtonElement.addEventListener('click', search);
    searchInputElement.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            search();
        }
    });
}
renderFilters(data, 'genre', genresElements);
renderFilters(data, 'publisher', publisherElements);
renderFilters(data, 'developer', developersElement);
renderFilters(data, 'platform', platformElements);
render(data);
eventListenerHandle();
